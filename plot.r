#!/usr/bin/Rscript

library(ggplot2)

lift_results <- read.csv("lift_results.csv", header=TRUE)
baseline_results <- read.csv("baseline_results.csv", header=TRUE)


# Add row and column convolution

lift_no_convoltion <- subset(lift_results, !grepl("convolution", lift_results$benchmark))
convolution_lift <- subset(lift_results, grepl("convolution", lift_results$benchmark))

summed <- aggregate(convolution_lift$time, by=list(convolution_lift$version, convolution_lift$size), FUN=function(x){ if (x[1] == 0 | x[2] == 0) 0 else x[1]+x[2] })

summed$benchmark <- "convolution"

names(summed) <- c("version", "size", "time", "benchmark")

all_results <- rbind(baseline_results, lift_no_convoltion, summed)

# Add gemv for atax and gesummv

atax <- subset(all_results, grepl("gemv", all_results$benchmark))

atax <- aggregate(atax$time, by=list(atax$version, atax$size), FUN=sum)

names(atax) <- c("version", "size", "time")
atax$benchmark <- "atax"

gesummv <- subset(all_results, grepl("gemv_N", all_results$benchmark))

gesummv$benchmark <- "gesummv"
gesummv$time <- gesummv$time*2

all_results <- subset(all_results, !grepl("gemv_N", all_results$benchmark))

all_results <- rbind(all_results, gesummv, atax)

all_results$normalised <- 0

reference <- subset(all_results, grepl("reference", all_results$version))
lift <- subset(all_results, !grepl("reference", all_results$version))

final_data <- subset(all_results, FALSE)

splitByBenchmark <- split(reference, reference$benchmark)
restSplitBenchmark <- split(lift, lift$benchmark)

for (j in seq(1, length(splitByBenchmark))) {

    versions <- splitByBenchmark[[j]]
    restBenchmark <- restSplitBenchmark[[j]]

    splitBySize <- split(versions, versions$size)
    restSplitBySize <- split(restBenchmark, restBenchmark$size)

    for (i in seq(1, length(splitBySize))) {

        thisSize <- splitBySize[[i]]
        restThisSize <- restSplitBySize[[i]]

        if (!grepl("^mm", names(splitByBenchmark)[j]))
            best <- thisSize$time
        else
            best <- subset(reference, benchmark == "mm" & size == names(splitBySize)[i])$time

        restThisSize$normalised <- best / restThisSize$time

        final_data <- rbind(final_data, restThisSize)
  }
}

final_data$version <- factor(final_data$version, levels=levels(final_data$version)[c(3,4,2)])
levels(final_data$version) <- c("None", "Barrier elimination\n+Control-flow simplification", "Barrier elinimation\n+Control-flow simplification\n+Array access simplification")

final_data$normalised[final_data$time == 0] <- 0

final_data$size <- factor(final_data$size, levels=rev(levels(final_data$size)))
levels(final_data$size) <- c("Small", "Large")

final_data$benchmark <- factor(final_data$benchmark, levels=levels(final_data$benchmark)[c(9, 8, 5, 4, 10, 7, 1, 16, 3, 15, 13, 14)])

levels(final_data$benchmark) <-
    c("N-Body, NVIDIA",
      "N-Body, AMD",
      "MD",
      "K-Means",
      "NN",
      "MRI-Q",
      "Convolution",
      "ATAX",
      "GEMV",
      "GESUMMV",
      "MM, AMD",
      "MM, NVIDIA")

ggplot(final_data, aes(x=size,fill=version,y=normalised)) + 
    geom_bar(stat="identity", colour="black", position=position_dodge()) + 
    facet_grid(.~benchmark) +
    scale_fill_brewer("Optimisations", palette="YlOrRd") +
    geom_hline(yintercept=1) +
    ylab("Speedup") +
    xlab("Input Size") +
    theme_bw() +
    theme(
       legend.position="top"
    )

ggsave("plot.pdf", width=14, height=3)
