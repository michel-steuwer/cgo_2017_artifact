#!/usr/bin/zsh

ROOT=${0:a:h}

SCRIPTS_ROOT=$ROOT/lift/scripts/Generate

benchmarks=('Convolution' 'Gemv' 'KMeans' 'MD' 'MMAMD' 'MRIQ' 'NBody' 'NN')

for b in $benchmarks
do

  echo "Generating code for ${b}..."

  ${SCRIPTS_ROOT}${b}

  APART_NO_ARITH_SIMPL=1 ${SCRIPTS_ROOT}${b}

  APART_NO_ARITH_SIMPL=1 APART_NO_BARRIER_ELIM=1 APART_NO_LOOP_OPT=1 ${SCRIPTS_ROOT}${b}

done

echo "Generating code for MMNvidia..."

APART_VECTOR_CAST=1 ${SCRIPTS_ROOT}MMNvidia

APART_VECTOR_CAST=1 APART_NO_ARITH_SIMPL=1 ${SCRIPTS_ROOT}MMNvidia

APART_VECTOR_CAST=1 APART_NO_ARITH_SIMPL=1 APART_NO_BARRIER_ELIM=1 APART_NO_LOOP_OPT=1 ${SCRIPTS_ROOT}MMNvidia

