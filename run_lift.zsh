#!/usr/bin/zsh

ROOT=${0:a:h}

PLATFORM=0
DEVICE=0

# GEMV, N

echo "Running GEMV, N..."

lift/scripts/MatrixVector --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel kernels/gemv/gemv_N.cl -p $PLATFORM -d $DEVICE > gemv_N_lift_small.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel kernels/gemv/gemv_N_no_simpl.cl -p $PLATFORM -d $DEVICE > gemv_N_no_simpl_lift_small.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel kernels/gemv/gemv_N_no_opt.cl -p $PLATFORM -d $DEVICE > gemv_N_no_opt_lift_small.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel kernels/gemv/gemv_N.cl -p $PLATFORM -d $DEVICE > gemv_N_lift_large.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel kernels/gemv/gemv_N_no_simpl.cl -p $PLATFORM -d $DEVICE > gemv_N_no_simpl_lift_large.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel kernels/gemv/gemv_N_no_opt.cl -p $PLATFORM -d $DEVICE > gemv_N_no_opt_lift_large.log

# GEMV, T

echo "Running GEMV, T..."

lift/scripts/MatrixVector --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel kernels/gemv/gemv_T.cl -p $PLATFORM -d $DEVICE > gemv_T_lift_small.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel kernels/gemv/gemv_T_no_simpl.cl -p $PLATFORM -d $DEVICE > gemv_T_no_simpl_lift_small.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 4096 -s 4096 -l 64 -g 4096 --loadKernel kernels/gemv/gemv_T_no_opt.cl -p $PLATFORM -d $DEVICE > gemv_T_no_opt_lift_small.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel kernels/gemv/gemv_T.cl -p $PLATFORM -d $DEVICE > gemv_T_lift_large.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel kernels/gemv/gemv_T_no_simpl.cl -p $PLATFORM -d $DEVICE > gemv_T_no_simpl_lift_large.log

lift/scripts/MatrixVector --variant 3 --ig --il -s 8192 -s 8192 -l 64 -g 8192 --loadKernel kernels/gemv/gemv_T_no_opt.cl -p $PLATFORM -d $DEVICE > gemv_T_no_opt_lift_large.log

# MM, Nvidia

echo "Running MM, Nvidia..."

lift/scripts/MatrixMultiplication --loadKernel kernels/mm_nvidia/us_clblast_mm.cl -p $PLATFORM -d $DEVICE -l 32 -l 8 -g 256 -g 128 -s 1024 -s 1024 -s 1024 > mm_nvidia_lift_small.log

# lift/scripts/MatrixMultiplication --loadKernel kernels/mm_nvidia/us_clblast_no_arith_simpl.cl -p $PLATFORM -d $DEVICE -l 32 -l 8 -g 256 -g 128 -s 1024 -s 1024 -s 1024 > mm_nvidia_no_simpl_lift_small.log

# lift/scripts/MatrixMultiplication --loadKernel kernels/mm_nvidia/us_clblast_no_opt.cl -p $PLATFORM -d $DEVICE -l 32 -l 8 -g 256 -g 128 -s 1024 -s 1024 -s 1024 > mm_nvidia_no_opt_lift_small.log

lift/scripts/MatrixMultiplication --loadKernel kernels/mm_nvidia/us_clblast_mm.cl -p $PLATFORM -d $DEVICE -l 32 -l 8 -g 1024 -g 512 -s 4096 -s 4096 -s 4096 > mm_nvidia_lift_large.log

# lift/scripts/MatrixMultiplication --loadKernel kernels/mm_nvidia/us_clblast_no_arith_simpl.cl -p $PLATFORM -d $DEVICE -l 32 -l 8 -g 1024 -g 512 -s 4096 -s 4096 -s 4096 > mm_nvidia_no_simpl_lift_large.log

# lift/scripts/MatrixMultiplication --loadKernel kernels/mm_nvidia/us_clblast_no_opt.cl -p $PLATFORM -d $DEVICE -l 32 -l 8 -g 1024 -g 512 -s 4096 -s 4096 -s 4096 > mm_nvidia_no_opt_lift_large.log

# MM, AMD

echo "Running MM, AMD..."

lift/scripts/MatrixMultiplication --loadKernel kernels/mm_amd/us_clblast_mm_hawaii.cl -l 32 -l 8 -g 256 -g 128 -p $PLATFORM -d $DEVICE -s 1024 -s 1024 -s 1024  > mm_amd_lift_small.log

lift/scripts/MatrixMultiplication --loadKernel kernels/mm_amd/us_clblast_mm_hawaii_no_simpl.cl -l 32 -l 8 -g 256 -g 128 -p $PLATFORM -d $DEVICE -s 1024 -s 1024 -s 1024 > mm_amd_no_simpl_lift_small.log

lift/scripts/MatrixMultiplication --loadKernel kernels/mm_amd/us_clblast_mm_hawaii_no_opt.cl -l 32 -l 8 -g 256 -g 128 -p $PLATFORM -d $DEVICE -s 1024 -s 1024 -s 1024 > mm_amd_no_opt_lift_small.log

lift/scripts/MatrixMultiplication --loadKernel kernels/mm_amd/us_clblast_mm_hawaii.cl -l 32 -l 8 -g 1024 -g 512 -p $PLATFORM -d $DEVICE -s 4096 -s 4096 -s 4096  > mm_amd_lift_large.log

lift/scripts/MatrixMultiplication --loadKernel kernels/mm_amd/us_clblast_mm_hawaii_no_simpl.cl -l 32 -l 8 -g 1024 -g 512 -p $PLATFORM -d $DEVICE -s 4096 -s 4096 -s 4096 > mm_amd_no_simpl_lift_large.log

lift/scripts/MatrixMultiplication --loadKernel kernels/mm_amd/us_clblast_mm_hawaii_no_opt.cl -l 32 -l 8 -g 1024 -g 512 -p $PLATFORM -d $DEVICE -s 4096 -s 4096 -s 4096 > mm_amd_no_opt_lift_large.log

# NBody, AMD

echo "Running NBody, AMD..."

lift/scripts/NBody --loadKernel kernels/nbody_amd/nbody_amd.cl -s 16384 -l 128 -g 16384 -p $PLATFORM -d $DEVICE > nbody_amd_lift_small.log

lift/scripts/NBody --loadKernel kernels/nbody_amd/nbody_amd_no_simpl.cl -s 16384 -l 128 -g 16384 -p $PLATFORM -d $DEVICE > nbody_amd_no_simpl_lift_small.log

lift/scripts/NBody --loadKernel kernels/nbody_amd/nbody_amd_no_opt.cl -s 16384 -l 128 -g 16384 -p $PLATFORM -d $DEVICE > nbody_amd_no_opt_lift_small.log

lift/scripts/NBody --loadKernel kernels/nbody_amd/nbody_amd.cl -s 131072 -l 128 -g 131072 -p $PLATFORM -d $DEVICE > nbody_amd_lift_large.log

lift/scripts/NBody --loadKernel kernels/nbody_amd/nbody_amd_no_simpl.cl -s 131072 -l 128 -g 131072 -p $PLATFORM -d $DEVICE > nbody_amd_no_simpl_lift_large.log

lift/scripts/NBody --loadKernel kernels/nbody_amd/nbody_amd_no_opt.cl -s 131072 -l 128 -g 131072 -p $PLATFORM -d $DEVICE > nbody_amd_no_opt_lift_large.log

# NBody, Nvidia

echo "Running NBody, Nvidia..."

lift/scripts/NBody --loadKernel kernels/nbody_nvidia/nbody_nvidia.cl -s 16384 -l 256 -g 16384 -p $PLATFORM -d $DEVICE > nbody_nvidia_lift_small.log

lift/scripts/NBody --loadKernel kernels/nbody_nvidia/nbody_nvidia_no_simpl.cl -s 16384 -l 256 -g 16384 -p $PLATFORM -d $DEVICE > nbody_nvidia_no_simpl_lift_small.log

lift/scripts/NBody --loadKernel kernels/nbody_nvidia/nbody_nvidia_no_opt.cl -s 16384 -l 256 -g 16384 -p $PLATFORM -d $DEVICE > nbody_nvidia_no_opt_lift_small.log

lift/scripts/NBody --loadKernel kernels/nbody_nvidia/nbody_nvidia.cl -s 131072 -l 256 -g 131072 -p $PLATFORM -d $DEVICE > nbody_nvidia_lift_large.log

lift/scripts/NBody --loadKernel kernels/nbody_nvidia/nbody_nvidia_no_simpl.cl -s 131072 -l 256 -g 131072 -p $PLATFORM -d $DEVICE > nbody_nvidia_no_simpl_lift_large.log

lift/scripts/NBody --loadKernel kernels/nbody_nvidia/nbody_nvidia_no_opt.cl -s 131072 -l 256 -g 131072 -p $PLATFORM -d $DEVICE > nbody_nvidia_no_opt_lift_large.log

# Convolution

echo "Running Convolution..."

lift/scripts/Convolution --variant 11 --loadKernel kernels/convolution/convolution_column_small.cl -l 16 -l 4 -g 512 -g 4096 -s 4096 -s 4096  -p $PLATFORM -d $DEVICE > convolution_column_lift_small.log

lift/scripts/Convolution --variant 11 --loadKernel kernels/convolution/convolution_column_small_no_simpl.cl -l 16 -l 4 -g 512 -g 4096 -s 4096 -s 4096  -p $PLATFORM -d $DEVICE > convolution_column_no_simpl_lift_small.log

lift/scripts/Convolution --variant 11 --loadKernel kernels/convolution/convolution_column_small_no_opt.cl -l 16 -l 4 -g 512 -g 4096 -s 4096 -s 4096  -p $PLATFORM -d $DEVICE > convolution_column_no_opt_lift_small.log


lift/scripts/Convolution --variant 13 --loadKernel kernels/convolution/convolution_column_large.cl -l 16 -l 4 -g 1024 -g 8192 -s 8192 -s 8192  -p $PLATFORM -d $DEVICE > convolution_column_lift_large.log

lift/scripts/Convolution --variant 13 --loadKernel kernels/convolution/convolution_column_large_no_simpl.cl -l 16 -l 4 -g 1024 -g 8192 -s 8192 -s 8192  -p $PLATFORM -d $DEVICE > convolution_column_no_simpl_lift_large.log

lift/scripts/Convolution --variant 13 --loadKernel kernels/convolution/convolution_column_large_no_opt.cl -l 16 -l 4 -g 1024 -g 8192 -s 8192 -s 8192  -p $PLATFORM -d $DEVICE > convolution_column_no_opt_lift_large.log




lift/scripts/Convolution --variant 8 --loadKernel kernels/convolution/convolution_row_small.cl -l 16 -l 8 -g 4096 -g 512 -s 4096 -s 4096 -p $PLATFORM -d $DEVICE > convolution_row_lift_small.log

lift/scripts/Convolution --variant 8 --loadKernel kernels/convolution/convolution_row_small_no_arith_opt.cl -l 16 -l 8 -g 4096 -g 512 -s 4096 -s 4096 -p $PLATFORM -d $DEVICE > convolution_row_no_simpl_lift_small.log

# lift/scripts/Convolution --variant 8 --loadKernel kernels/convolution/convolution_row_small_no_opt.cl -l 16 -l 8 -g 4096 -g 512 -s 4096 -s 4096 -p $PLATFORM -d $DEVICE > convolution_row_no_opt_lift_small.log


lift/scripts/Convolution --variant 14 --loadKernel kernels/convolution/convolution_row_large.cl -l 16 -l 8 -g 8192 -g 1024 -s 8192 -s 8192 -p $PLATFORM -d $DEVICE > convolution_row_lift_large.log

lift/scripts/Convolution --variant 14 --loadKernel kernels/convolution/convolution_row_large_no_arith_opt.cl -l 16 -l 8 -g 8192 -g 1024 -s 8192 -s 8192 -p $PLATFORM -d $DEVICE > convolution_row_no_simpl_lift_large.log

# lift/scripts/Convolution --variant 14 --loadKernel kernels/convolution/convolution_row_large_no_opt.cl -l 16 -l 8 -g 8192 -g 1024 -s 8192 -s 8192 -p $PLATFORM -d $DEVICE > convolution_row_no_opt_lift_large.log

# K-Means

echo "Running K-Means..."

lift/scripts/KMeans --loadKernel kernels/kmeans/kmeans.cl -s 204800 -s 5 -s  -l 256 -g 204800 -p $PLATFORM -d $DEVICE > kmeans_lift_small.log

lift/scripts/KMeans --loadKernel kernels/kmeans/kmeans_no_simpl.cl -s 204800 -s 34 -s 5 -l 256 -g 204800 -p $PLATFORM -d $DEVICE > kmeans_no_simpl_lift_small.log

lift/scripts/KMeans --loadKernel kernels/kmeans/kmeans_no_opt.cl -s 204800 -s 5 -s 34 -l 256 -g 204800 -p $PLATFORM -d $DEVICE > kmeans_no_opt_lift_small.log

lift/scripts/KMeans --loadKernel kernels/kmeans/kmeans.cl -s 819200 -s 5 -s 34 -l 256 -g 819200 -p $PLATFORM -d $DEVICE > kmeans_lift_large.log

lift/scripts/KMeans --loadKernel kernels/kmeans/kmeans_no_simpl.cl -s 819200 -s 5 -s 34 -l 256 -g 819200 -p $PLATFORM -d $DEVICE > kmeans_no_simpl_lift_large.log

lift/scripts/KMeans --loadKernel kernels/kmeans/kmeans_no_opt.cl -s 819200 -s 5 -s 34 -l 256 -g 819200 -p $PLATFORM -d $DEVICE > kmeans_no_opt_lift_large.log

# Nearest Neighbour

echo "Running NN..."

lift/scripts/NearestNeighbour --loadKernel kernels/nn/nn.cl -s 8388608 -g 8388608 -l 128 -p $PLATFORM -d $DEVICE > nn_lift_small.log 

lift/scripts/NearestNeighbour --loadKernel kernels/nn/nn_no_simpl.cl -s 8388608 -g 8388608 -l 128 -p $PLATFORM -d $DEVICE > nn_no_simpl_lift_small.log 

lift/scripts/NearestNeighbour --loadKernel kernels/nn/nn_no_opt.cl -s 8388608 -g 8388608 -l 128 -p $PLATFORM -d $DEVICE > nn_no_opt_lift_small.log 

lift/scripts/NearestNeighbour --loadKernel kernels/nn/nn.cl -s 33554432 -g 33554432 -l 128 -p $PLATFORM -d $DEVICE > nn_lift_large.log 

lift/scripts/NearestNeighbour --loadKernel kernels/nn/nn_no_simpl.cl -s 33554432 -g 33554432 -l 128 -p $PLATFORM -d $DEVICE > nn_no_simpl_lift_large.log 

lift/scripts/NearestNeighbour --loadKernel kernels/nn/nn_no_opt.cl -s 33554432 -g 33554432 -l 128 -p $PLATFORM -d $DEVICE > nn_no_opt_lift_large.log 

# MRI-Q

echo "Running MRI-Q..."

mriq_log="$ROOT/mriq_lift_small.log"
mriq_no_simpl_log="$ROOT/mriq_no_simpl_lift_small.log"
mriq_no_opt_log="$ROOT/mriq_no_opt_lift_small.log"

mriq_large_log="$ROOT/mriq_lift_large.log"
mriq_no_simpl_large_log="$ROOT/mriq_no_simpl_lift_large.log"
mriq_no_opt_large_log="$ROOT/mriq_no_opt_lift_large.log"

rm -f $mriq_log
rm -f $mriq_no_simpl_log
rm -f $mriq_no_opt_log

rm -f $mriq_large_log
rm -f $mriq_no_simpl_large_log
rm -f $mriq_no_opt_large_log

cd $ROOT/parboil/benchmarks/mri-q
for i in `seq 1 10`
do
  build/opencl_default/mri-q -p $PLATFORM -d $DEVICE -i $ROOT/parboil/datasets/mri-q/small/input/32_32_32_dataset.bin -o $ROOT/parboil/benchmarks/mri-q/run/small/32_32_32_dataset.out -k lift >> $mriq_log
done

for i in `seq 1 10`
do
  build/opencl_default/mri-q -p $PLATFORM -d $DEVICE -i $ROOT/parboil/datasets/mri-q/small/input/32_32_32_dataset.bin -o $ROOT/parboil/benchmarks/mri-q/run/small/32_32_32_dataset.out -k lift_no_simpl >> $mriq_no_simpl_log
done

for i in `seq 1 10`
do
  build/opencl_default/mri-q -p $PLATFORM -d $DEVICE -i $ROOT/parboil/datasets/mri-q/small/input/32_32_32_dataset.bin -o $ROOT/parboil/benchmarks/mri-q/run/small/32_32_32_dataset.out -k lift_no_opt >> $mriq_no_opt_log
done


for i in `seq 1 10`
do
  build/opencl_default/mri-q -p $PLATFORM -d $DEVICE -i $ROOT/parboil/datasets/mri-q/large/input/64_64_64_dataset.bin -o $ROOT/parboil/benchmarks/mri-q/run/large/64_64_64_dataset.out -k lift >> $mriq_large_log
done

for i in `seq 1 10`
do
  build/opencl_default/mri-q -p $PLATFORM -d $DEVICE -i $ROOT/parboil/datasets/mri-q/large/input/64_64_64_dataset.bin -o $ROOT/parboil/benchmarks/mri-q/run/large/64_64_64_dataset.out -k lift_no_simpl >> $mriq_no_simpl_large_log
done

for i in `seq 1 10`
do
  build/opencl_default/mri-q -p $PLATFORM -d $DEVICE -i $ROOT/parboil/datasets/mri-q/large/input/64_64_64_dataset.bin -o $ROOT/parboil/benchmarks/mri-q/run/large/64_64_64_dataset.out -k lift_no_opt >> $mriq_no_opt_large_log
done



# Molecular Dynamics

echo "Running MD..."

cd $ROOT/shoc/src/opencl/level1/md

./MD --device $DEVICE --platform $PLATFORM --iterations 10 --size 1 -k lift > $ROOT/md_lift_small.log

./MD --device $DEVICE --platform $PLATFORM --iterations 10 --size 1 -k lift_no_arith_opt > $ROOT/md_no_simpl_lift_small.log

./MD --device $DEVICE --platform $PLATFORM --iterations 10 --size 1 -k lift_no_opt > $ROOT/md_no_opt_lift_small.log

./MD --device $DEVICE --platform $PLATFORM --iterations 10 --size 4 -k lift > $ROOT/md_lift_large.log

./MD --device $DEVICE --platform $PLATFORM --iterations 10 --size 4 -k lift_no_arith_opt > $ROOT/md_no_simpl_lift_large.log

./MD --device $DEVICE --platform $PLATFORM --iterations 10 --size 4 -k lift_no_opt > $ROOT/md_no_opt_lift_large.log

cd $ROOT
