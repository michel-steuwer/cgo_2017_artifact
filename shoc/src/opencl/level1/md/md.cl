#ifdef SINGLE_PRECISION
#define POSVECTYPE float4
#define FORCEVECTYPE float4
#define FPTYPE float
#elif K_DOUBLE_PRECISION
#pragma OPENCL EXTENSION cl_khr_fp64: enable
#define POSVECTYPE double4
#define FORCEVECTYPE double4
#define FPTYPE double
#elif AMD_DOUBLE_PRECISION
#pragma OPENCL EXTENSION cl_amd_fp64: enable
#define POSVECTYPE double4
#define FORCEVECTYPE double4
#define FPTYPE double
#endif

float4 updateF(float4 f, float4 ipos, float4 jpos, float cutsq, float lj1, float lj2){
  {
  // Calculate distance
  float delx = ipos.x - jpos.x;
  float dely = ipos.y - jpos.y;
  float delz = ipos.z - jpos.z;
  float r2inv = delx*delx + dely*dely + delz*delz;
  // If distance is less than cutoff, calculate force
  if (r2inv < cutsq) {
    r2inv = 1.0f/r2inv;
    float r6inv = r2inv * r2inv * r2inv;
    float forceC = r2inv*r6inv*(lj1*r6inv - lj2);
    f.x += delx * forceC;
    f.y += dely * forceC;
    f.z += delz * forceC;
  }
  return f;
}
  }
float4 id4(float4 x){
  { return x; }}
kernel void lift(global float* v__20, const global float* restrict v__7, int v_M_1, const global int* restrict v__8,  float v__9, float v__10, float v__11, int v_N_0){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float4 v__15;
  float4 v__14;
  /* Private Memory */
  {
    int v_wg_id_6 = get_group_id(0);
    {
      {
        int v_l_id_5 = get_local_id(0);
        {
          /* let: */
          v__14 = vload4(((128 * v_wg_id_6) + v_l_id_5),v__7);
          float4 v__32 = 0.0f;
          v__15 = v__32;
          {
            /* reduce_seq */
            for (int v_i_3 = 0; v_i_3 < v_M_1; v_i_3 += 1) {
              {
                v__15 = updateF(v__15, v__14, vload4(v__8[((128 * v_wg_id_6) + (v_i_3 * v_N_0) + v_l_id_5)],v__7), v__9, v__10, v__11);
              }
            }
            /* end reduce_seq */
          }
          /* map_seq */
          {
            int v_i_4 = 0;
            {
              vstore4(id4(v__15),((128 * v_wg_id_6) + v_l_id_5),v__20);
            }
          }
          /* end map_seq */
        }
      }
    }
  }
}
}

kernel void lift_no_arith_opt(global float* v__20, const global float* restrict v__7, int v_M_1, const global int* restrict v__8,  float v__9, float v__10, float v__11, int v_N_0){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float4 v__15;
  float4 v__14;
  /* Private Memory */
  {
    int v_wg_id_6 = get_group_id(0);
    {
      {
        int v_l_id_5 = get_local_id(0);
        {
          /* let: */
          v__14 = vload4(((0 + (((128 * v_wg_id_6) + v_l_id_5) * 4)) / 4),v__7);
          float4 v__32 = 0.0f;
          v__15 = v__32;
          {
            /* reduce_seq */
            for (int v_i_3 = 0; v_i_3 < v_M_1; v_i_3 += 1) {
              {
                v__15 = updateF(v__15, v__14, vload4(((0 + (4 * v__8[((0 + ((((((((128 * v_wg_id_6) + v_l_id_5) * v_M_1) + v_i_3) / v_M_1) + ((((((128 * v_wg_id_6) + v_l_id_5) * v_M_1) + v_i_3) % v_M_1) * v_N_0)) / v_N_0) * (1 * v_N_0))) + ((((((((128 * v_wg_id_6) + v_l_id_5) * v_M_1) + v_i_3) / v_M_1) + ((((((128 * v_wg_id_6) + v_l_id_5) * v_M_1) + v_i_3) % v_M_1) * v_N_0)) % v_N_0) * 1))])) / 4),v__7), v__9, v__10, v__11);
              }
            }
            /* end reduce_seq */
          }
          /* map_seq */
          {
            int v_i_4 = 0;
            {
              vstore4(id4(v__15),(((0 + (((128 * v_wg_id_6) + v_l_id_5) * (1 * 4))) + (4 * v_i_4)) / 4),v__20);
            }
          }
          /* end map_seq */
        }
      }
    }
  }
}
}

kernel void lift_no_opt(global float* v__20, const global float* restrict v__7, int v_M_1, const global int* restrict v__8,  float v__9, float v__10, float v__11, int v_N_0){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float4 v__15;
  float4 v__14;
  /* Private Memory */
  for (int v_wg_id_6 = get_group_id(0); v_wg_id_6 < (v_N_0 / (128)); v_wg_id_6 += get_num_groups(0)) {
    {
      for (int v_l_id_5 = get_local_id(0); v_l_id_5 < 128; v_l_id_5 += get_local_size(0)) {
        {
          /* let: */
          v__14 = vload4(((0 + (((128 * v_wg_id_6) + v_l_id_5) * 4)) / 4),v__7);
          float4 v__32 = 0.0f;
          v__15 = v__32;
          {
            /* reduce_seq */
            for (int v_i_3 = 0; v_i_3 < v_M_1; v_i_3 += 1) {
              {
                v__15 = updateF(v__15, v__14, vload4(((0 + (4 * v__8[((0 + ((((((((128 * v_wg_id_6) + v_l_id_5) * v_M_1) + v_i_3) / v_M_1) + ((((((128 * v_wg_id_6) + v_l_id_5) * v_M_1) + v_i_3) % v_M_1) * v_N_0)) / v_N_0) * (1 * v_N_0))) + ((((((((128 * v_wg_id_6) + v_l_id_5) * v_M_1) + v_i_3) / v_M_1) + ((((((128 * v_wg_id_6) + v_l_id_5) * v_M_1) + v_i_3) % v_M_1) * v_N_0)) % v_N_0) * 1))])) / 4),v__7), v__9, v__10, v__11);
              }
            }
            /* end reduce_seq */
          }
          /* map_seq */
          for (int v_i_4 = 0; v_i_4 < 1; v_i_4 += 1) {
            {
              vstore4(id4(v__15),(((0 + (((128 * v_wg_id_6) + v_l_id_5) * (1 * 4))) + (4 * v_i_4)) / 4),v__20);
            }
          }
          /* end map_seq */
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
    }
  }
}
}

kernel void lift_no_barrier_opt(global float* v__20, const global float* restrict v__7, int v_M_1, const global int* restrict v__8,  float v__9, float v__10, float v__11, int v_N_0){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float4 v__15;
  float4 v__14;
  /* Private Memory */
  {
    int v_wg_id_6 = get_group_id(0);
    {
      {
        int v_l_id_5 = get_local_id(0);
        {
          /* let: */
          v__14 = vload4(((128 * v_wg_id_6) + v_l_id_5),v__7);
          float4 v__25 = 0.0f;
          v__15 = v__25;
          {
            /* reduce_seq */
            for (int v_i_3 = 0; v_i_3 < v_M_1; v_i_3 += 1) {
              {
                v__15 = updateF(v__15, v__14, vload4(v__8[((128 * v_wg_id_6) + (v_i_3 * v_N_0) + v_l_id_5)],v__7), v__9, v__10, v__11);
              }
            }
            /* end reduce_seq */
          }
          /* map_seq */
          {
            int v_i_4 = 0;
            {
              vstore4(id4(v__15),((128 * v_wg_id_6) + v_l_id_5),v__20);
            }
          }
          /* end map_seq */
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
    }
  }
}
}

kernel void lift_no_loop_opt(global float* v__20, const global float* restrict v__7, int v_M_1, const global int* restrict v__8,  float v__9, float v__10, float v__11, int v_N_0){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float4 v__15;
  float4 v__14;
  /* Private Memory */
  for (int v_wg_id_6 = get_group_id(0); v_wg_id_6 < (v_N_0 / (128)); v_wg_id_6 += get_num_groups(0)) {
    {
      for (int v_l_id_5 = get_local_id(0); v_l_id_5 < 128; v_l_id_5 += get_local_size(0)) {
        {
          /* let: */
          v__14 = vload4(((128 * v_wg_id_6) + v_l_id_5),v__7);
          float4 v__32 = 0.0f;
          v__15 = v__32;
          {
            /* reduce_seq */
            for (int v_i_3 = 0; v_i_3 < v_M_1; v_i_3 += 1) {
              {
                v__15 = updateF(v__15, v__14, vload4(v__8[((128 * v_wg_id_6) + (v_i_3 * v_N_0) + v_l_id_5)],v__7), v__9, v__10, v__11);
              }
            }
            /* end reduce_seq */
          }
          /* map_seq */
          for (int v_i_4 = 0; v_i_4 < 1; v_i_4 += 1) {
            {
              vstore4(id4(v__15),((128 * v_wg_id_6) + v_l_id_5),v__20);
            }
          }
          /* end map_seq */
        }
      }
    }
  }
}
}

__kernel void compute_lj_force(__global FORCEVECTYPE *force,
                               __global POSVECTYPE *position,
                               const int neighCount,
                               __global int* neighList,
                               const FPTYPE cutsq,
                               const FPTYPE lj1,
                               const FPTYPE lj2,
                               const int inum)
{
    uint idx = get_global_id(0);

    POSVECTYPE ipos = position[idx];
    FORCEVECTYPE f = {0.0f, 0.0f, 0.0f, 0.0f};

    int j = 0;
    while (j < neighCount)
    {
        int jidx = neighList[j*inum + idx];

        // Uncoalesced read
        POSVECTYPE jpos = position[jidx];

        // Calculate distance
        FPTYPE delx = ipos.x - jpos.x;
        FPTYPE dely = ipos.y - jpos.y;
        FPTYPE delz = ipos.z - jpos.z;
        FPTYPE r2inv = delx*delx + dely*dely + delz*delz;

        // If distance is less than cutoff, calculate force
        if (r2inv < cutsq)
        {
            r2inv = 1.0f/r2inv;
            FPTYPE r6inv = r2inv * r2inv * r2inv;
            FPTYPE forceC = r2inv*r6inv*(lj1*r6inv - lj2);

            f.x += delx * forceC;
            f.y += dely * forceC;
            f.z += delz * forceC;
        }
        j++;
    }
    // store the results
    force[idx] = f;
}
