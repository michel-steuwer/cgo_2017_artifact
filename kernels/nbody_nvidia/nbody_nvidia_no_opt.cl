#ifndef Tuple_float4_float4_DEFINED
#define Tuple_float4_float4_DEFINED
typedef struct {
  float4 _0;
  float4 _1;
} Tuple_float4_float4;
#endif
#ifndef Tuple_Tuple_float4_float4_float4_DEFINED
#define Tuple_Tuple_float4_float4_float4_DEFINED
typedef struct {
  Tuple_float4_float4 _0;
  float4 _1;
} Tuple_Tuple_float4_float4_float4;
#endif

#ifndef Tuple_float4_float4_DEFINED
#define Tuple_float4_float4_DEFINED
typedef struct {
  float4 _0;
  float4 _1;
} Tuple_float4_float4;
#endif

float4 id(float4 x){
  { return x; }
}
float4 calcAcc(float4 p1, float4 p2, float deltaT, float espSqr, float4 acc){
  {
  float4 r;
  r.xyz = p2.xyz - p1.xyz ;
  float distSqr = r.x*r.x + r.y*r.y + r.z*r.z;
  float invDist = 1.0f / sqrt(distSqr + espSqr);
  float invDistCube = invDist * invDist * invDist;
  float s = invDistCube * p2.w;
  float4 res;
  res.xyz = acc.xyz + s * r.xyz;
  return res;
}
 
}
Tuple_float4_float4 update(float4 pos, float4 vel, float deltaT, float4 acceleration){
  typedef Tuple_float4_float4 Tuple;
  
  {
  float4 newPos;
  newPos.xyz = pos.xyz + vel.xyz * deltaT + 0.5f * acceleration.xyz * deltaT * deltaT;
  newPos.w = pos.w;
  float4 newVel;
  newVel.xyz = vel.xyz + acceleration.xyz * deltaT;
  newVel.w = vel.w;
  Tuple t = {newPos, newVel};
  return t;
}
        
}
kernel void KERNEL(const global float* restrict v__56, const global float* restrict v__57, float v__58, float v__59, global Tuple_float4_float4* v__89, int v_N_0){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__73[1024];
  /* Typed Value memory */
  float4 v__66;
  /* Private Memory */
  float4 v__64_0;
  
  float4 v__71_0;
  
  for (int v_wg_id_42 = get_group_id(1);v_wg_id_42<1;v_wg_id_42 = (1 + v_wg_id_42)){
    for (int v_wg_id_43 = get_group_id(0);v_wg_id_43<(v_N_0 / (256));v_wg_id_43 = (v_wg_id_43 + (v_N_0 / (256)))){
      /* unroll */
      v__64_0 = id(vload4(((0 + (4 * ((((0 * 256) + get_local_id(0)) + (256 * v_wg_id_43)) + (v_N_0 * v_wg_id_42)))) / 4),v__56));
      /* end unroll */
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
      
      float4 v_tmp_94 = 0.0f;
      v__66 = v_tmp_94;
      /* unroll */
      /* unroll */
      v__71_0 = id(v__66);
      /* end unroll */
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
      
      /* end unroll */
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
      
      /* reduce_seq */
      for (int v_i_48 = 0;v_i_48<(v_N_0 / (256));v_i_48 = (1 + v_i_48)){
        for (int v_l_id_49 = get_local_id(1);v_l_id_49<1;v_l_id_49 = (1 + v_l_id_49)){
          for (int v_l_id_50 = get_local_id(0);v_l_id_50<256;v_l_id_50 = (256 + v_l_id_50)){
            vstore4(id(vload4(((0 + (4 * (v_l_id_50 + (256 * (v_l_id_49 + (1 * v_i_48)))))) / 4),v__56)),(((0 + (v_l_id_49 * (4 * 256))) + (4 * v_l_id_50)) / 4),v__73);;
          }
          barrier(CLK_LOCAL_MEM_FENCE);
          
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        
        /* unroll */
        /* unroll */
        /* reduce_seq */
        for (int v_i_53 = 0;v_i_53<256;v_i_53 = (1 + v_i_53)){
          v__71_0 = calcAcc(v__64_0, vload4((((0 + (((0 * 1) + get_local_id(1)) * (4 * 256))) + (4 * v_i_53)) / 4),v__73), v__59, v__58, v__71_0);
        }
        /* end reduce_seq */
        /* end unroll */
        barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
        
        /* end unroll */
        barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
        
      }
      /* end reduce_seq */
      /* unroll */
      /* unroll */
      v__89[((0 + (((((0 * 1) + get_local_id(1)) + (v_wg_id_43 * (1 * 1))) + (v_wg_id_42 * ((1 * 1) * (v_N_0 / (256))))) * (1 * 256))) + (1 * ((0 * 256) + get_local_id(0))))] = update(v__64_0, vload4(((0 + (4 * ((((0 * 256) + get_local_id(0)) + (256 * v_wg_id_43)) + (v_N_0 * v_wg_id_42)))) / 4),v__57), v__59, v__71_0);
      /* end unroll */
      barrier(CLK_GLOBAL_MEM_FENCE);
      
      /* end unroll */
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}

