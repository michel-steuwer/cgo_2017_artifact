#ifndef Tuple_float4_float4_DEFINED
#define Tuple_float4_float4_DEFINED
typedef struct {
  float4 _0;
  float4 _1;
} Tuple_float4_float4;
#endif
#ifndef Tuple_Tuple_float4_float4_float4_DEFINED
#define Tuple_Tuple_float4_float4_float4_DEFINED
typedef struct {
  Tuple_float4_float4 _0;
  float4 _1;
} Tuple_Tuple_float4_float4_float4;
#endif

#ifndef Tuple_float4_float4_DEFINED
#define Tuple_float4_float4_DEFINED
typedef struct {
  float4 _0;
  float4 _1;
} Tuple_float4_float4;
#endif

float4 id(float4 x){
  { return x; }
}
float4 calcAcc(float4 p1, float4 p2, float deltaT, float espSqr, float4 acc){
  {
  float4 r;
  r.xyz = p2.xyz - p1.xyz ;
  float distSqr = r.x*r.x + r.y*r.y + r.z*r.z;
  float invDist = 1.0f / sqrt(distSqr + espSqr);
  float invDistCube = invDist * invDist * invDist;
  float s = invDistCube * p2.w;
  float4 res;
  res.xyz = acc.xyz + s * r.xyz;
  return res;
}

}
Tuple_float4_float4 update(float4 pos, float4 vel, float deltaT, float4 acceleration){
  typedef Tuple_float4_float4 Tuple;

  {
  float4 newPos;
  newPos.xyz = pos.xyz + vel.xyz * deltaT + 0.5f * acceleration.xyz * deltaT * deltaT;
  newPos.w = pos.w;
  float4 newVel;
  newVel.xyz = vel.xyz + acceleration.xyz * deltaT;
  newVel.w = vel.w;
  Tuple t = {newPos, newVel};
  return t;
}

}
kernel void KERNEL(const global float* restrict v__39, const global float* restrict v__40, float v__41, float v__42, global Tuple_float4_float4* v__73, int v_N_0){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__57[1024] __attribute__ ((aligned(16)));
  /* Typed Value memory */
  float4 v__50;
  /* Private Memory */
  float4 v__48_0;

  float4 v__55_0;

  /* iteration count is exactly 1, no loop emitted */
  {
    int v_wg_id_25 = get_group_id(1);
    /* iteration count is exactly 1, no loop emitted */
    {
      int v_wg_id_26 = get_group_id(0);
      /* unroll */
      v__48_0 = id(vload4(((0 + (4 * ((((0 * 256) + get_local_id(0)) + (256 * v_wg_id_26)) + (v_N_0 * v_wg_id_25)))) / 4),v__39));
      /* end unroll */
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

      float4 v_tmp_136 = 0.0f;
      v__50 = v_tmp_136;
      /* unroll */
      /* unroll */
      v__55_0 = id(v__50);
      /* end unroll */
      /* end unroll */
      /* reduce_seq */
      for (int v_i_31 = 0;v_i_31<(v_N_0 / (256));v_i_31 = (1 + v_i_31)){
        /* iteration count is exactly 1, no loop emitted */
        {
          int v_l_id_32 = get_local_id(1);
          /* iteration count is exactly 1, no loop emitted */
          {
            int v_l_id_33 = get_local_id(0);
            vstore4(id(vload4(((0 + (4 * (v_l_id_33 + (256 * (v_l_id_32 + (1 * v_i_31)))))) / 4),v__39)),(((0 + (v_l_id_32 * (4 * 256))) + (4 * v_l_id_33)) / 4),v__57);;
          }
        }
        barrier(CLK_LOCAL_MEM_FENCE);

        /* unroll */
        /* unroll */
        /* reduce_seq */
        for (int v_i_36 = 0;v_i_36<256;v_i_36 = (1 + v_i_36)){
          v__55_0 = calcAcc(v__48_0, vload4((((0 + (((0 * 1) + get_local_id(1)) * (4 * 256))) + (4 * v_i_36)) / 4),v__57), v__42, v__41, v__55_0);
        }
        /* end reduce_seq */
        /* end unroll */
        /* end unroll */
        barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);

      }
      /* end reduce_seq */
      /* unroll */
      /* unroll */
      v__73[((0 + (((((0 * 1) + get_local_id(1)) + (v_wg_id_26 * (1 * 1))) + (v_wg_id_25 * ((1 * 1) * (v_N_0 / (256))))) * (1 * 256))) + (1 * ((0 * 256) + get_local_id(0))))] = update(v__48_0, vload4(((0 + (4 * ((((0 * 256) + get_local_id(0)) + (256 * v_wg_id_26)) + (v_N_0 * v_wg_id_25)))) / 4),v__40), v__42, v__55_0);
      /* end unroll */
      /* end unroll */
    }
  }
}}
