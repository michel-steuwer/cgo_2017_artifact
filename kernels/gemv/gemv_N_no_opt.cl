#ifndef Tuple_float_float_DEFINED
#define Tuple_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
} Tuple_float_float;
#endif

float id(float x){
  { return x; }
}
float multAndSumUp(float acc, float l, float r){
  { return acc + (l * r); }
}
float add(float x, float y){
  { return x+y; }
}
float mult(float l, float r){
  { return l * r; }
}
kernel void KERNEL(const global float* restrict v__26, const global float* restrict v__27, const global float* restrict v__28, float v__29, float v__30, global float* v__60, int v_M_1, int v_N_0){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__40[64] __attribute__ ((aligned(16)));
  /* Typed Value memory */
  float v__33;
  /* Private Memory */
  float v__37_0;
  
  float v__55_0;
  
  float v__58_0;
  
  for (int v_wg_id_18 = get_group_id(0);v_wg_id_18<(v_M_1 / (64));v_wg_id_18 = (v_wg_id_18 + get_num_groups(0))){
    float v_tmp_77 = 0.0f;
    v__33 = v_tmp_77;
    /* unroll */
    v__37_0 = id(v__33);
    /* end unroll */
    barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
    
    /* reduce_seq */
    for (int v_i_20 = 0;v_i_20<(v_N_0 / (64));v_i_20 = (1 + v_i_20)){
      for (int v_l_id_21 = get_local_id(0);v_l_id_21<64;v_l_id_21 = (64 + v_l_id_21)){
        v__40[(0 + (1 * v_l_id_21))] = id(v__27[(0 + (1 * (v_l_id_21 + (64 * v_i_20))))]);
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      /* unroll */
      /* reduce_seq */
      for (int v_i_23 = 0;v_i_23<64;v_i_23 = (1 + v_i_23)){
        v__37_0 = multAndSumUp(v__37_0, v__26[((0 + ((((((((0 * 64) + get_local_id(0)) + (64 * v_i_20)) / 64) + (((((0 * 64) + get_local_id(0)) + (64 * v_i_20)) % 64) * (v_N_0 / (64)))) / (v_N_0 / (64))) + (64 * v_wg_id_18)) * (1 * v_N_0))) + (1 * (v_i_23 + (64 * ((((((0 * 64) + get_local_id(0)) + (64 * v_i_20)) / 64) + (((((0 * 64) + get_local_id(0)) + (64 * v_i_20)) % 64) * (v_N_0 / (64)))) % (v_N_0 / (64)))))))], v__40[(0 + (1 * v_i_23))]);
      }
      /* end reduce_seq */
      /* end unroll */
      barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE);
      
    }
    /* end reduce_seq */
    /* map_seq */
    /* unroll */
    /* unroll */
    v__55_0 = mult(v__37_0, v__29);
    v__58_0 = mult(v__28[(0 + (1 * (((0 * 64) + get_local_id(0)) + (64 * v_wg_id_18))))], v__30);
    v__60[((0 + ((0 + (1 * v_wg_id_18)) * (1 * 64))) + (1 * ((0 * 64) + get_local_id(0))))] = add(v__55_0, v__58_0);
    /* end unroll */
    barrier(CLK_GLOBAL_MEM_FENCE);
    
    /* end unroll */
    /* end map_seq */
  }
}}
