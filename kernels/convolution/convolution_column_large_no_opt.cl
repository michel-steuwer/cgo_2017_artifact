#ifndef Tuple_float_float_DEFINED
#define Tuple_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
} Tuple_float_float;
#endif

float id(float x){
  { return x; }
}
float multAndSumUp(float acc, float l, float r){
  { return acc + (l * r); }
}
kernel void KERNEL(const global float* restrict v__126, const global float* restrict v__127, global float* v__134){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__128[576];
  /* Typed Value memory */
  float v__129;
  /* Private Memory */
  for (int v_wg_id_118 = get_group_id(1);v_wg_id_118<(((0 + (0 + 8192)) + (-1 * 0)) / 4);v_wg_id_118 = (2048 + v_wg_id_118)){
    for (int v_wg_id_119 = get_group_id(0);v_wg_id_119<(((8 + (8 + 8192)) + (-1 * 16)) / 128);v_wg_id_119 = (64 + v_wg_id_119)){
      for (int v_l_id_120 = get_local_id(1);v_l_id_120<4;v_l_id_120 = (4 + v_l_id_120)){
        for (int v_l_id_121 = get_local_id(0);v_l_id_121<144;v_l_id_121 = (16 + v_l_id_121)){
          v__128[((0 + (v_l_id_120 * (1 * 144))) + (1 * v_l_id_121))] = id(v__126[((0 + ((1 * 8192) * ( (((((((v_l_id_120 + (4 * v_wg_id_119)) / 4) + ((((8 + (8 + 8192)) + (-1 * 16)) / 128) * ((v_l_id_120 + (4 * v_wg_id_119)) % 4))) / (((8 + (8 + 8192)) + (-1 * 16)) / 128)) + (4 * v_wg_id_118)) + (-1 * 0)) >= 0) ? ( (((((((v_l_id_120 + (4 * v_wg_id_119)) / 4) + ((((8 + (8 + 8192)) + (-1 * 16)) / 128) * ((v_l_id_120 + (4 * v_wg_id_119)) % 4))) / (((8 + (8 + 8192)) + (-1 * 16)) / 128)) + (4 * v_wg_id_118)) + (-1 * 0)) < 8192) ? ((((((v_l_id_120 + (4 * v_wg_id_119)) / 4) + ((((8 + (8 + 8192)) + (-1 * 16)) / 128) * ((v_l_id_120 + (4 * v_wg_id_119)) % 4))) / (((8 + (8 + 8192)) + (-1 * 16)) / 128)) + (4 * v_wg_id_118)) + (-1 * 0)) : (8192 + (-1 * 1)) ) : 0 ))) + (1 * ( (((v_l_id_121 + (128 * ((((v_l_id_120 + (4 * v_wg_id_119)) / 4) + ((((8 + (8 + 8192)) + (-1 * 16)) / 128) * ((v_l_id_120 + (4 * v_wg_id_119)) % 4))) % (((8 + (8 + 8192)) + (-1 * 16)) / 128)))) + (-1 * 8)) >= 0) ? ( (((v_l_id_121 + (128 * ((((v_l_id_120 + (4 * v_wg_id_119)) / 4) + ((((8 + (8 + 8192)) + (-1 * 16)) / 128) * ((v_l_id_120 + (4 * v_wg_id_119)) % 4))) % (((8 + (8 + 8192)) + (-1 * 16)) / 128)))) + (-1 * 8)) < 8192) ? ((v_l_id_121 + (128 * ((((v_l_id_120 + (4 * v_wg_id_119)) / 4) + ((((8 + (8 + 8192)) + (-1 * 16)) / 128) * ((v_l_id_120 + (4 * v_wg_id_119)) % 4))) % (((8 + (8 + 8192)) + (-1 * 16)) / 128)))) + (-1 * 8)) : (8192 + (-1 * 1)) ) : 0 )))]);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_122 = get_local_id(1);v_l_id_122<((4 + (-1 * 0)) / 1);v_l_id_122 = (4 + v_l_id_122)){
        for (int v_l_id_123 = get_local_id(0);v_l_id_123<((144 + (-1 * 16)) / 1);v_l_id_123 = (16 + v_l_id_123)){
          float v_tmp_147 = 0.0f;
          v__129 = v_tmp_147;
          /* reduce_seq */
          /* unroll */
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((0 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((0 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((0 % 17) + (1 * (((((0 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((0 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (0 * 1))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((1 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((1 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((1 % 17) + (1 * (((((1 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((1 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 1))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((2 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((2 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((2 % 17) + (1 * (((((2 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((2 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 2))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((3 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((3 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((3 % 17) + (1 * (((((3 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((3 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 3))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((4 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((4 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((4 % 17) + (1 * (((((4 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((4 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 4))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((5 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((5 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((5 % 17) + (1 * (((((5 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((5 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 5))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((6 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((6 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((6 % 17) + (1 * (((((6 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((6 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 6))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((7 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((7 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((7 % 17) + (1 * (((((7 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((7 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 7))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((8 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((8 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((8 % 17) + (1 * (((((8 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((8 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 8))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((9 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((9 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((9 % 17) + (1 * (((((9 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((9 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 9))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((10 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((10 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((10 % 17) + (1 * (((((10 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((10 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 10))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((11 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((11 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((11 % 17) + (1 * (((((11 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((11 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 11))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((12 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((12 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((12 % 17) + (1 * (((((12 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((12 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 12))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((13 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((13 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((13 % 17) + (1 * (((((13 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((13 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 13))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((14 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((14 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((14 % 17) + (1 * (((((14 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((14 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 14))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((15 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((15 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((15 % 17) + (1 * (((((15 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((15 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 15))]);
          v__129 = multAndSumUp(v__129, v__128[((0 + (((((((16 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((16 / 17) + (1 * v_l_id_123)) % 1))) / ((144 + (-1 * 16)) / 1)) + (1 * v_l_id_122)) * (1 * 144))) + (1 * ((16 % 17) + (1 * (((((16 / 17) + (1 * v_l_id_123)) / 1) + (((144 + (-1 * 16)) / 1) * (((16 / 17) + (1 * v_l_id_123)) % 1))) % ((144 + (-1 * 16)) / 1))))))], v__127[(0 + (1 * 16))]);
          /* end unroll */
          /* end reduce_seq */
          /* map_seq */
          /* unroll */
          v__134[(((0 + ((((((v_l_id_122 + (v_wg_id_119 * ((4 + (-1 * 0)) / 1))) / ((4 + (-1 * 0)) / 1)) + ((((8 + (8 + 8192)) + (-1 * 16)) / 128) * ((v_l_id_122 + (v_wg_id_119 * ((4 + (-1 * 0)) / 1))) % ((4 + (-1 * 0)) / 1)))) / (((8 + (8 + 8192)) + (-1 * 16)) / 128)) + (v_wg_id_118 * ((4 + (-1 * 0)) / 1))) * (1 * (1 * (((144 + (-1 * 16)) / 1) * (((8 + (8 + 8192)) + (-1 * 16)) / 128)))))) + ((v_l_id_123 + (((144 + (-1 * 16)) / 1) * ((((v_l_id_122 + (v_wg_id_119 * ((4 + (-1 * 0)) / 1))) / ((4 + (-1 * 0)) / 1)) + ((((8 + (8 + 8192)) + (-1 * 16)) / 128) * ((v_l_id_122 + (v_wg_id_119 * ((4 + (-1 * 0)) / 1))) % ((4 + (-1 * 0)) / 1)))) % (((8 + (8 + 8192)) + (-1 * 16)) / 128)))) * (1 * 1))) + (0 * 1))] = id(v__129);
          /* end unroll */
          /* end map_seq */
        }
        barrier(CLK_GLOBAL_MEM_FENCE);
        
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}

