#!/usr/bin/zsh

ROOT=${0:a:h}

get_kernel_ms="awk '/Kernel time \(ms\)/ { print \"kernel,\",\$4 }'"
get_median_ms="awk '/MEDIAN/ { print \"kernel,\",\$2 }'"

median() {
  sort -n $1 | awk ' { a[i++]=$1; } END { x=int((i+1)/2); if (x < (i+1)/2) print (a[x-1]+a[x])/2; else print a[x-1]; }'
}

echo "Parsing results..."

benchmarks_not_scala=('mriq' 'md')

benchmarks=('nbody_nvidia' 'nbody_amd' 'kmeans' 'nn' 'mm_nvidia' 'mm_amd' 'convolution_row' 'convolution_column' 'gemv_N' 'gemv_T')

versions=('lift' 'no_simpl_lift' 'no_opt_lift')

sizes=('small' 'large')

results_file="lift_results.csv"

rm -f $results_file

echo "benchmark,time,version,size" > $results_file

for b in $benchmarks
do
  for s in $sizes
  do
    for v in $versions
    do

      log_name="${b}_${v}_${s}.log"
      times_name="${b}_${v}_${s}_kernel.log"

      eval "$get_median_ms $log_name" > $times_name

      median_kernel=$(awk -F , '/kernel/ { print $2 }' $times_name | median)

      echo "$b,$median_kernel,${v},${s}" >> $results_file
    done
  done
done

for b in ${benchmarks_not_scala}
do
  for s in $sizes
  do
    for v in $versions
    do

      log_name="${b}_${v}_${s}.log"
      times_name="${b}_${v}_${s}_kernel.log"

      eval "$get_kernel_ms $log_name" > $times_name

      median_kernel=$(awk -F , '/kernel/ { print $2 }' $times_name | median)

      echo "$b,$median_kernel,${v},${s}" >> $results_file
    done
  done
done
