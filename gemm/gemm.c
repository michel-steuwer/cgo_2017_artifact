#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <clblast_c.h>
#include <CL/cl.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>

int compare_function(const void *a,const void *b) {
  double *x = (double *) a;
  double *y = (double *) b;
  if (*x < *y) return -1;
  else if (*x > *y) return 1; return 0;
}

#define CL_CHECK_ERROR(err) do{if (err) {printf("FATAL ERROR %d at " __FILE__":%d\n",err,__LINE__); exit(1); } } while(0)

int main(int argc, char** argv)
{

  if (argc < 7) {
    printf("%s -p platform -d device -s size\n", argv[0]);
    exit(1);
  }

  const int platform = atoi(argv[2]);
  const int device = atoi(argv[4]);
  const int M = atoi(argv[6]);
  const int K = M;
  const int N = M;

  float alpha = 1.0f;
  float beta = 0.0f;
  float* A = (float*)malloc(sizeof(float) * M * K);
  float* B = (float*)malloc(sizeof(float) * K * N);
  float* C = (float*)malloc(sizeof(float) * M * N);

  for (int i = 0; i < M; i++) {
    for (int j = 0; j < K; ++j) {
      A[i * K + j] = (float)( (i + j) % 10 );
    }
  }

  for (int i = 0; i < K; i++) {
    for (int j = 0; j < N; ++j) {
      B[i * N + j] = (float)( (i + j) % 10 );
    }
  }

  for (int i = 0; i < M; i++) {
    for (int j = 0; j < N; ++j) {
      C[i * N + j] = (float) 0;
    }
  }

  cl_int cl_error;

  cl_uint num_platforms;
  cl_error = clGetPlatformIDs(0, NULL, &num_platforms);
  CL_CHECK_ERROR(cl_error);

  cl_platform_id* all_platforms = (cl_platform_id*) malloc(sizeof(cl_platform_id)*num_platforms);
  cl_error = clGetPlatformIDs(num_platforms, all_platforms, NULL);
  CL_CHECK_ERROR(cl_error);

  cl_platform_id chosen_platform = all_platforms[platform];

  cl_uint num_devices;
  cl_error = clGetDeviceIDs(chosen_platform, CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
  CL_CHECK_ERROR(cl_error);

  cl_device_id* all_devices = (cl_device_id*) malloc(sizeof(cl_device_id)*num_devices);
  cl_error = clGetDeviceIDs(chosen_platform, CL_DEVICE_TYPE_ALL, num_devices, all_devices, NULL);
  CL_CHECK_ERROR(cl_error);

  cl_device_id chosen_device = all_devices[device];

  cl_context context = clCreateContext(NULL, 1, &chosen_device, NULL, NULL, &cl_error);
  CL_CHECK_ERROR(cl_error);

  cl_command_queue queue = clCreateCommandQueue(context, chosen_device, CL_QUEUE_PROFILING_ENABLE, &cl_error);
  CL_CHECK_ERROR(cl_error);

  cl_mem d_A = clCreateBuffer(context, CL_MEM_READ_ONLY,  M*K*sizeof(float), NULL, &cl_error);
  CL_CHECK_ERROR(cl_error);
  cl_mem d_B = clCreateBuffer(context, CL_MEM_READ_ONLY,  N*K*sizeof(float), NULL, &cl_error);
  CL_CHECK_ERROR(cl_error);
  cl_mem d_C = clCreateBuffer(context, CL_MEM_READ_WRITE, M*N*sizeof(float), NULL, &cl_error);
  CL_CHECK_ERROR(cl_error);

  cl_error = clEnqueueWriteBuffer(queue, d_A, CL_TRUE, 0, M*K*sizeof(float), A, 0, NULL, NULL);
  CL_CHECK_ERROR(cl_error);
  cl_error = clEnqueueWriteBuffer(queue, d_B, CL_TRUE, 0, N*K*sizeof(float), B, 0, NULL, NULL);
  CL_CHECK_ERROR(cl_error);
  cl_error = clEnqueueWriteBuffer(queue, d_C, CL_TRUE, 0, M*N*sizeof(float), C, 0, NULL, NULL);
  CL_CHECK_ERROR(cl_error);

  int iter = 10;
  double time = 0.0;
  double* times = (double*)malloc(iter * sizeof(double));
  for (int i = 0; i < iter; ++i) {

    cl_event event;
    
    StatusCode stat = CLBlastSgemm(kRowMajor, kYes, kNo, M, N, K, alpha, d_A, 0, M, d_B, 0, K, beta, d_C, 0, M, &queue, &event);

    clWaitForEvents(1, &event);

    if (stat != kSuccess)
      exit(1);

    cl_ulong start, end;

    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_START, sizeof(start), &start, NULL);
    clGetEventProfilingInfo(event, CL_PROFILING_COMMAND_END, sizeof(end), &end, NULL);

    float t = (end - start) / 1000.0 / 1000.0;
    time += t;
    times[i] = (t);
  }

  qsort(times,iter,sizeof(double),compare_function);

  time = time / iter;

  printf("Kernel time (ms): %f\n", times[iter/2]);

  free(times);

  free(A);
  free(B);
  free(C);
  return 0;
}

