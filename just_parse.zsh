#!/usr/bin/zsh

ROOT=${0:a:h}

get_kernel_ms="awk '/Kernel time \(ms\)/ { print \"kernel,\",\$4 }'"

median() {
  sort -n $1 | awk ' { a[i++]=$1; } END { x=int((i+1)/2); if (x < (i+1)/2) print (a[x-1]+a[x])/2; else print a[x-1]; }'
}

cd $ROOT

echo "Parsing results..."

benchmarks=('nbody_nvidia' 'nbody_amd' 'kmeans' 'nn' 'mriq' 'md' 'mm' 'conv' 'gemv_N' 'gemv_T')

results_file="baseline_results.csv"

rm -f $results_file

echo "benchmark,time,version" > $results_file

for b in $benchmarks
do
  
  log_name="${b}.log"
  times_name="${b}_kernel.log"

  eval "$get_kernel_ms $log_name" > $times_name

  median_kernel=$(awk -F , '/kernel/ { print $2 }' $times_name | median)

  echo "$b,$median_kernel,reference" >> $results_file

done
