#!/usr/bin/zsh

ROOT=${0:a:h}

echo "Unpacking Rodinia..."

if [[ ! -d rodinia_3.1 ]]
then
  tar xf rodinia_3.1.tar.bz2
  if [[ $? != 0 ]]
  then
    echo "Failed to unpack Rodinia... Exiting..."
    exit
  fi
fi

echo "Unpacking NVIDIA SDK..."

if [[ ! -d NVIDIA_GPU_Computing_SDK ]]
then
  tar xf nvidia.tar.bz2
  if [[ $? != 0 ]]
  then
    echo "Failed to unpack NVIDIA SDK... Exiting..."
    exit
  fi
fi

echo "Unpacking AMD SDK..."

if [[ ! -d AMDAPP ]]
then
  tar xf amd.tar.bz2
  if [[ $? != 0 ]]
  then
    echo "Failed to unpack AMD SDK... Exiting..."
    exit
  fi
fi

echo "Building CLBlast..."

cd $ROOT/CLBlast
mkdir -p build && cd build
cmake -DCMAKE_INSTALL_PREFIX=$ROOT ..
make -j 4
make install

echo "Building NVIDIA..."

cd $ROOT/NVIDIA_GPU_Computing_SDK/OpenCL/src/oclNbody
make

cd $ROOT/NVIDIA_GPU_Computing_SDK/OpenCL/src/oclConvolutionSeparable
make

echo "Building AMD..."

cd $ROOT/AMDAPP/samples/opencl/cl/NBody
mkdir -p build && cd build
cmake ..
make

echo "Building Rodinia..."
cd $ROOT/rodinia_3.1/opencl/kmeans
make
cd $ROOT/rodinia_3.1/opencl/nn
make

echo "Building Parboil..."

cd $ROOT/parboil
./parboil compile mri-q opencl

echo "Building shoc..."

cd $ROOT/shoc
./configure
make
cd $ROOT/shoc/src/opencl/level1/md
make

echo "Building blas..."

cd $ROOT/gemv_N
make

cd $ROOT/gemv_T
make

cd $ROOT/gemm
make


echo "All done..."
