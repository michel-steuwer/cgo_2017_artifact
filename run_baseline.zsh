#!/usr/bin/zsh

ROOT=${0:a:h}

PLATFORM=0
DEVICE=0

export LD_LIBRARY_PATH=$ROOT/lib:$LD_LIBRARY_PATH

nbody_nvidia_log=$ROOT/nbody_nvidia_small.log
nbody_amd_log=$ROOT/nbody_amd_small.log
kmeans_log=$ROOT/kmeans_small.log
nn_log=$ROOT/nn_small.log
md_log=$ROOT/md_small.log
mriq_log=$ROOT/mriq_small.log
conv_log=$ROOT/convolution_small.log
mm_log=$ROOT/mm_small.log
gemv_N_log=$ROOT/gemv_N_small.log
gemv_T_log=$ROOT/gemv_T_small.log

# N-Body, Nvidia

echo "Running N-Body, Nvidia..."

rm -f $nbody_nvidia_log
rm -f $ROOT/nbody_nvidia_large.log

cd $ROOT/NVIDIA_GPU_Computing_SDK/OpenCL
bin/linux/release/oclNbody --n=16384 --p=$PLATFORM --d=$DEVICE >> $nbody_nvidia_log

bin/linux/release/oclNbody --n=131072 --p=$PLATFORM --d=$DEVICE >> $ROOT/nbody_nvidia_large.log

# N-Body, AMD

echo "Running N-Body, AMD..."

rm -f $nbody_amd_log
rm -f $ROOT/nbody_amd_large.log

cd $ROOT/AMDAPP/samples/opencl/cl/NBody
bin/x86_64/Release/NBody -x 16384 -t -i 10 --platformId $PLATFORM --deviceId $DEVICE >> $nbody_amd_log

bin/x86_64/Release/NBody -x 131072 -t -i 10 --platformId $PLATFORM --deviceId $DEVICE >> $ROOT/nbody_amd_large.log

# K-Means
#
echo "Running K-Means..."

rm -f $kmeans_log
rm -f $ROOT/kmeans_large.log

cd $ROOT/rodinia_3.1/opencl/kmeans
./kmeans -i ../../data/kmeans/204800.txt -p $PLATFORM -d $DEVICE >> $kmeans_log

./kmeans -i ../../data/kmeans/819200.txt -p $PLATFORM -d $DEVICE >> $ROOT/kmeans_large.log 

# Nearest Neighbour

echo "Running Nearest Neighbour..."

rm -f $nn_log
rm -f $ROOT/nn_large.log

cd $ROOT/rodinia_3.1/opencl/nn
for i in `seq 1 10`
do
  ./nn filelist_small -r 5 -lat 30 -lng 90 -p $PLATFORM -d $DEVICE -t >> $nn_log
done

cd $ROOT/rodinia_3.1/opencl/nn
for i in `seq 1 10`
do
  ./nn filelist_large -r 5 -lat 30 -lng 90 -p $PLATFORM -d $DEVICE -t >> $ROOT/nn_large.log
done

# MD

echo "Running Molecular Dynamics..."

rm -f $md_log
rm -f $ROOT/md_large.log

cd $ROOT/shoc/src/opencl/level1/md
./MD --device $DEVICE --platform $PLATFORM --iterations 10 --size 1 >> $md_log

./MD --device $DEVICE --platform $PLATFORM --iterations 10 --size 4 >> $ROOT/md_large.log

# MRI-Q

echo "Running MRI-Q..."

rm -f $mriq_log
rm -f $ROOT/mriq_large.log

cd $ROOT/parboil/benchmarks/mri-q
for i in `seq 1 10`
do
  build/opencl_default/mri-q -p $PLATFORM -d $DEVICE -i $ROOT/parboil/datasets/mri-q/small/input/32_32_32_dataset.bin -o $ROOT/parboil/benchmarks/mri-q/run/small/32_32_32_dataset.out >> $mriq_log
done

for i in `seq 1 10`
do
  build/opencl_default/mri-q -p $PLATFORM -d $DEVICE -i $ROOT/parboil/datasets/mri-q/large/input/64_64_64_dataset.bin -o $ROOT/parboil/benchmarks/mri-q/run/large/64_64_64_dataset.out >> $ROOT/mriq_large.log
done

# Convolution

echo "Running Convolution..."

rm -f $conv_log
rm -f $ROOT/convolution_large.log

cd $ROOT/NVIDIA_GPU_Computing_SDK/OpenCL
bin/linux/release/oclConvolutionSeparable -p $PLATFORM -d $DEVICE -s  4096 >> $conv_log

bin/linux/release/oclConvolutionSeparable -p $PLATFORM -d $DEVICE -s  8192 >> $ROOT/convolution_large.log

# MM

echo "Running Matrix Multiplication..."

rm -f $mm_log
rm -f $ROOT/mm_large.log

cd $ROOT/gemm
./gemm -p $PLATFORM -d $DEVICE -s 1024 >> $mm_log

./gemm -p $PLATFORM -d $DEVICE -s 4096 >> $ROOT/mm_large.log

# GEMV N

echo "Running GEMV N..."

rm -f $gemv_N_log
rm -f $ROOT/gemv_N_large.log

cd $ROOT/gemv_N
./gemv -p $PLATFORM -d $DEVICE -s 4096 >> $gemv_N_log

./gemv -p $PLATFORM -d $DEVICE -s 8192 >> $ROOT/gemv_N_large.log

# GEMV T

echo "Running GEMV T..."

rm -f $gemv_T_log
rm -f $ROOT/gemv_T_large.log

cd $ROOT/gemv_T
./gemv -p $PLATFORM -d $DEVICE -s 4096 >> $gemv_T_log

./gemv -p $PLATFORM -d $DEVICE -s 8192 >> $ROOT/gemv_T_large.log


# Parse logs

get_kernel_ms="awk '/Kernel time \(ms\)/ { print \"kernel,\",\$4 }'"

median() {
  sort -n $1 | awk ' { a[i++]=$1; } END { x=int((i+1)/2); if (x < (i+1)/2) print (a[x-1]+a[x])/2; else print a[x-1]; }'
}

cd $ROOT

echo "Parsing results..."

benchmarks=('nbody_nvidia' 'nbody_amd' 'kmeans' 'nn' 'mriq' 'md' 'mm' 'convolution' 'gemv_N' 'gemv_T')

sizes=('small' 'large')

results_file="baseline_results.csv"

rm -f $results_file

echo "benchmark,time,version,size" > $results_file

for b in $benchmarks
do
  for s in $sizes
  do

    log_name="${b}_${s}.log"
    times_name="${b}_${s}_kernel.log"

    eval "$get_kernel_ms $log_name" > $times_name

    median_kernel=$(awk -F , '/kernel/ { print $2 }' $times_name | median)

    echo "$b,$median_kernel,reference,$s" >> $results_file

  done

done
