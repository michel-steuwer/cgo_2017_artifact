#include "macros.h"

//#define NC  4
//#define COARSE_GENERAL
// #define COARSE_SPEC NC

#ifndef Tuple_float_float_DEFINED
#define Tuple_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
} Tuple_float_float;
#endif
#ifndef Tuple_float_float_float_float_DEFINED
#define Tuple_float_float_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
  float _2;
  float _3;
} Tuple_float_float_float_float;
#endif
#ifndef Tuple_float_float_float_float_float_DEFINED
#define Tuple_float_float_float_float_float_DEFINED
typedef struct {
  float _0;
  float _1;
  float _2;
  float _3;
  float _4;
} Tuple_float_float_float_float_float;
#endif

float phiMag(float phiR, float phiI){
  { return phiR * phiR + phiI * phiI; }}
kernel void ComputePhiMag_lift(const global float* restrict v__7, const global float* restrict v__8, global float* v__12, int v_K_0){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  {
    int v_gl_id_2 = get_global_id(0);
    {
      v__12[v_gl_id_2] = phiMag(v__7[v_gl_id_2], v__8[v_gl_id_2]);
    }
  }
}
}

kernel void ComputePhiMag_lift_no_arith_opt(const global float* restrict v__7, const global float* restrict v__8, global float* v__12, int v_K_0){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  {
    int v_gl_id_2 = get_global_id(0);
    {
      v__12[(0 + (1 * v_gl_id_2))] = phiMag(v__7[(0 + (1 * v_gl_id_2))], v__8[(0 + (1 * v_gl_id_2))]);
    }
  }
}
}


kernel void ComputePhiMag_lift_no_loop_opt(const global float* restrict v__7, const global float* restrict v__8, global float* v__12, int v_K_0){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  for (int v_gl_id_2 = get_global_id(0); v_gl_id_2 < v_K_0; v_gl_id_2 += get_global_size(0)) {
    {
      v__12[v_gl_id_2] = phiMag(v__7[v_gl_id_2], v__8[v_gl_id_2]);
    }
  }
}
}

kernel void ComputePhiMag_lift_no_opt(const global float* restrict v__7, const global float* restrict v__8, global float* v__12, int v_K_0){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  for (int v_gl_id_2 = get_global_id(0); v_gl_id_2 < v_K_0; v_gl_id_2 += get_global_size(0)) {
    {
      v__12[(0 + (1 * v_gl_id_2))] = phiMag(v__7[(0 + (1 * v_gl_id_2))], v__8[(0 + (1 * v_gl_id_2))]);
    }
  }
}
}

__kernel void
ComputePhiMag_GPU(__global float* phiR, __global float* phiI, __global float* phiMag, int numK) {
  int indexK = get_global_id(0);
  if (indexK < numK) {
    float real = phiR[indexK];
    float imag = phiI[indexK];
    phiMag[indexK] = real*real + imag*imag;
  }
}

Tuple_float_float pair(float x, float y){
  typedef Tuple_float_float Tuple;
  { Tuple t = {x, y}; return t; }}

Tuple_float_float computeQ(float sX, float sY, float sZ, float Kx, float Ky, float Kz, float PhiMag, Tuple_float_float acc){
  typedef Tuple_float_float Tuple;
  {
    #define PIx2 6.2831853071795864769252867665590058f
    float expArg = PIx2 * (Kx * sX + Ky * sY + Kz * sZ);
    acc._0 = acc._0 + PhiMag * cos(expArg);
    acc._1 = acc._1 + PhiMag * sin(expArg);

    return acc;
}}

Tuple_float_float id(Tuple_float_float x){
  typedef Tuple_float_float Tuple;
  { return x; }}
kernel void ComputeQ_lift(int v_K_0, int bla, const global float* restrict v__20, const global float* restrict v__21, const global float* restrict v__22, const global float* restrict v__23, const global float* restrict v__24, const global Tuple_float_float_float_float* restrict v__25, global Tuple_float_float* v__36, int v_X_3){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__29;
  float v__28;
  float v__27;
  /* Private Memory */
  Tuple_float_float v__32_0;
  {
    int v_gl_id_6 = get_global_id(0);
    {
      /* let: */
      v__27 = v__20[v_gl_id_6];
      /* let: */
      v__28 = v__21[v_gl_id_6];
      /* let: */
      v__29 = v__22[v_gl_id_6];
      v__32_0 = pair(v__23[v_gl_id_6], v__24[v_gl_id_6]);
      {
        /* reduce_seq */
        for (int v_i_4 = 0; v_i_4 < v_K_0; v_i_4 += 1) {
          {
            v__32_0 = computeQ(v__27, v__28, v__29, v__25[v_i_4]._0, v__25[v_i_4]._1, v__25[v_i_4]._2, v__25[v_i_4]._3, v__32_0);
          }
        }
        /* end reduce_seq */
      }
      /* map_seq */
      /* unroll */
      v__36[v_gl_id_6] = id(v__32_0);
      /* end unroll */
      /* end map_seq */
    }
  }
}
}

kernel void ComputeQ_lift_no_loop_opt(int v_K_0, int bla, const global float* restrict v__20, const global float* restrict v__21, const global float* restrict v__22, const global float* restrict v__23, const global float* restrict v__24, const global Tuple_float_float_float_float* restrict v__25, global Tuple_float_float* v__36, int v_X_3){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__29;
  float v__28;
  float v__27;
  /* Private Memory */
  Tuple_float_float v__32_0;
  for (int v_gl_id_6 = get_global_id(0); v_gl_id_6 < v_X_3; v_gl_id_6 += get_global_size(0)) {
    {
      /* let: */
      v__27 = v__20[v_gl_id_6];
      /* let: */
      v__28 = v__21[v_gl_id_6];
      /* let: */
      v__29 = v__22[v_gl_id_6];
      v__32_0 = pair(v__23[v_gl_id_6], v__24[v_gl_id_6]);
      {
        /* reduce_seq */
        for (int v_i_4 = 0; v_i_4 < v_K_0; v_i_4 += 1) {
          {
            v__32_0 = computeQ(v__27, v__28, v__29, v__25[v_i_4]._0, v__25[v_i_4]._1, v__25[v_i_4]._2, v__25[v_i_4]._3, v__32_0);
          }
        }
        /* end reduce_seq */
      }
      /* map_seq */
      /* unroll */
      v__36[v_gl_id_6] = id(v__32_0);
      /* end unroll */
      /* end map_seq */
    }
  }
}
}

kernel void ComputeQ_lift_no_arith_opt(int v_K_0, int bla, const global float* restrict v__20, const global float* restrict v__21, const global float* restrict v__22, const global float* restrict v__23, const global float* restrict v__24, const global Tuple_float_float_float_float* restrict v__25, global Tuple_float_float* v__36, int v_X_3){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__29;
  float v__28;
  float v__27;
  /* Private Memory */
  Tuple_float_float v__32_0;
  {
    int v_gl_id_6 = get_global_id(0);
    {
      /* let: */
      v__27 = v__20[(0 + (1 * v_gl_id_6))];
      /* let: */
      v__28 = v__21[(0 + (1 * v_gl_id_6))];
      /* let: */
      v__29 = v__22[(0 + (1 * v_gl_id_6))];
      v__32_0 = pair(v__23[(0 + (1 * v_gl_id_6))], v__24[(0 + (1 * v_gl_id_6))]);
      {
        /* reduce_seq */
        for (int v_i_4 = 0; v_i_4 < v_K_0; v_i_4 += 1) {
          {
            v__32_0 = computeQ(v__27, v__28, v__29, v__25[(0 + (1 * v_i_4))]._0, v__25[(0 + (1 * v_i_4))]._1, v__25[(0 + (1 * v_i_4))]._2, v__25[(0 + (1 * v_i_4))]._3, v__32_0);
          }
        }
        /* end reduce_seq */
      }
      /* map_seq */
      /* unroll */
      v__36[((0 + ((1 * 1) * v_gl_id_6)) + (0 * 1))] = id(v__32_0);
      /* end unroll */
      /* end map_seq */
    }
  }
}
}

kernel void ComputeQ_lift_no_opt(int v_K_0, int bla, const global float* restrict v__20, const global float* restrict v__21, const global float* restrict v__22, const global float* restrict v__23, const global float* restrict v__24, const global Tuple_float_float_float_float* restrict v__25, global Tuple_float_float* v__36, int v_X_3){
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__29;
  float v__28;
  float v__27;
  /* Private Memory */
  Tuple_float_float v__32_0;
  for (int v_gl_id_6 = get_global_id(0); v_gl_id_6 < v_X_3; v_gl_id_6 += get_global_size(0)) {
    {
      /* let: */
      v__27 = v__20[(0 + (1 * v_gl_id_6))];
      /* let: */
      v__28 = v__21[(0 + (1 * v_gl_id_6))];
      /* let: */
      v__29 = v__22[(0 + (1 * v_gl_id_6))];
      v__32_0 = pair(v__23[(0 + (1 * v_gl_id_6))], v__24[(0 + (1 * v_gl_id_6))]);
      {
        /* reduce_seq */
        for (int v_i_4 = 0; v_i_4 < v_K_0; v_i_4 += 1) {
          {
            v__32_0 = computeQ(v__27, v__28, v__29, v__25[(0 + (1 * v_i_4))]._0, v__25[(0 + (1 * v_i_4))]._1, v__25[(0 + (1 * v_i_4))]._2, v__25[(0 + (1 * v_i_4))]._3, v__32_0);
          }
        }
        /* end reduce_seq */
      }
      /* map_seq */
      /* unroll */
      v__36[((0 + ((1 * 1) * v_gl_id_6)) + (0 * 1))] = id(v__32_0);
      /* end unroll */
      /* end map_seq */
    }
  }
}
}

__kernel void
ComputeQ_GPU(int numK, int kGlobalIndex,
	     __global float* x, __global float* y, __global float* z,
	     __global float* Qr, __global float* Qi, __global struct kValues* ck)
{
#ifdef COARSE_GENERAL

  float sX[NC];
  float sY[NC];
  float sZ[NC];
  float sQr[NC];
  float sQi[NC];

  #pragma unroll
  for (int tx = 0; tx < NC; tx++) {
    int xIndex = get_group_id(0)*KERNEL_Q_THREADS_PER_BLOCK + NC * get_local_id(0) + tx;

    sX[tx] = x[xIndex];
    sY[tx] = y[xIndex];
    sZ[tx] = z[xIndex];
    sQr[tx] = Qr[xIndex];
    sQi[tx] = Qi[xIndex];
  }

  // Loop over all elements of K in constant mem to compute a partial value
  // for X.
  int kIndex = 0;
  for (; (kIndex < KERNEL_Q_K_ELEMS_PER_GRID) && (kGlobalIndex < numK);
       kIndex ++, kGlobalIndex ++) {
    float kx = ck[kIndex].Kx;
    float ky = ck[kIndex].Ky;
    float kz = ck[kIndex].Kz;
    float pm = ck[kIndex].PhiMag;

    #pragma unroll
    for (int tx = 0; tx < NC; tx++) {
      float expArg = PIx2 *
                   (kx * sX[tx] +
                    ky * sY[tx] +
                    kz * sZ[tx]);
      sQr[tx] += pm * cos(expArg);
      sQi[tx] += pm * sin(expArg);
    }
  }

  #pragma unroll
  for (int tx = 0; tx < NC; tx++) {
    int xIndex = get_group_id(0)*KERNEL_Q_THREADS_PER_BLOCK + NC * get_local_id(0) + tx;
    Qr[xIndex] = sQr[tx];
    Qi[xIndex] = sQi[tx];
  }

#elif (COARSE_SPEC==2)

  float2 sX;
  float2 sY;
  float2 sZ;
  float2 sQr;
  float2 sQi;

  {
    int xIndex = get_group_id(0)*KERNEL_Q_THREADS_PER_BLOCK + NC*get_local_id(0);

    sX = *(((__global float2*)(x + xIndex)));
    sY = *(((__global float2*)(y + xIndex)));
    sZ = *(((__global float2*)(z + xIndex)));
    sQr = *(((__global float2*)(Qr + xIndex)));
    sQi = *(((__global float2*)(Qi + xIndex)));
  }

  // Loop over all elements of K in constant mem to compute a partial value
  // for X.
  int kIndex = 0;
  for (; (kIndex < KERNEL_Q_K_ELEMS_PER_GRID) && (kGlobalIndex < numK);
       kIndex += 1, kGlobalIndex += 1) {
    float kx = ck[kIndex].Kx;
    float ky = ck[kIndex].Ky;
    float kz = ck[kIndex].Kz;
    float pm = ck[kIndex].PhiMag;

    // #pragma unroll
    float expArg;
    expArg = PIx2 *
                   (kx * sX.x +
                    ky * sY.x +
                    kz * sZ.x);
    sQr.x += pm * cos(expArg);
    sQi.x += pm * sin(expArg);
    expArg = PIx2 *
                   (kx * sX.y +
                    ky * sY.y +
                    kz * sZ.y);
    sQr.y += pm * cos(expArg);
    sQi.y += pm * sin(expArg);
  }

  {
    int xIndex = get_group_id(0)*KERNEL_Q_THREADS_PER_BLOCK + NC * get_local_id(0);
    *((__global float2*)(Qr + xIndex)) = sQr;
    *((__global float2*)(Qi + xIndex)) = sQi;
  }

#elif (COARSE_SPEC==4)

  float4 sX;
  float4 sY;
  float4 sZ;
  float4 sQr;
  float4 sQi;

  {
    int xIndex = get_group_id(0)*KERNEL_Q_THREADS_PER_BLOCK + NC*get_local_id(0);

    sX = *((__global float4*)(x + xIndex));
    sY = *((__global float4*)(y + xIndex));
    sZ = *((__global float4*)(z + xIndex));
    sQr = *((__global float4*)(Qr + xIndex));
    sQi = *((__global float4*)(Qi + xIndex));
  }

  // Loop over all elements of K in constant mem to compute a partial value
  // for X.
  int kIndex = 0;
  for (; (kIndex < KERNEL_Q_K_ELEMS_PER_GRID) && (kGlobalIndex < numK);
       kIndex += 1, kGlobalIndex += 1) {
    float kx = ck[kIndex].Kx;
    float ky = ck[kIndex].Ky;
    float kz = ck[kIndex].Kz;
    float pm = ck[kIndex].PhiMag;

    // #pragma unroll
    float4 expArg;
    expArg.x = PIx2 *
                   (kx * sX.x +
                    ky * sY.x +
                    kz * sZ.x);
    sQr.x += pm * cos(expArg.x);
    sQi.x += pm * sin(expArg.x);
    expArg.y = PIx2 *
                   (kx * sX.y +
                    ky * sY.y +
                    kz * sZ.y);
    sQr.y += pm * cos(expArg.y);
    sQi.y += pm * sin(expArg.y);
    expArg.z = PIx2 *
                   (kx * sX.z +
                    ky * sY.z +
                    kz * sZ.z);
    sQr.z += pm * cos(expArg.z);
    sQi.z += pm * sin(expArg.z);
    expArg.w = PIx2 *
                   (kx * sX.w +
                    ky * sY.w +
                    kz * sZ.w);
    sQr.w += pm * cos(expArg.w);
    sQi.w += pm * sin(expArg.w);
  }

  {
    int xIndex = get_group_id(0)*KERNEL_Q_THREADS_PER_BLOCK + NC * get_local_id(0);
    *((__global float4*)(Qr + xIndex)) = sQr;
    *((__global float4*)(Qi + xIndex)) = sQi;
  }

#else

// Uncoarse

#ifdef UNROLL_2X

  float sX;
  float sY;
  float sZ;
  float sQr;
  float sQi;

  // Determine the element of the X arrays computed by this thread
  int xIndex = get_group_id(0)*KERNEL_Q_THREADS_PER_BLOCK + get_local_id(0);

  // Read block's X values from global mem to shared mem
  sX = x[xIndex];
  sY = y[xIndex];
  sZ = z[xIndex];
  sQr = Qr[xIndex];
  sQi = Qi[xIndex];

  // Loop over all elements of K in constant mem to compute a partial value
  // for X.
  int kIndex = 0;
  if (numK % 2) {
    float expArg = PIx2 * (ck[0].Kx * sX + ck[0].Ky * sY + ck[0].Kz * sZ);
    sQr += ck[0].PhiMag * cos(expArg);
    sQi += ck[0].PhiMag * sin(expArg);
    kIndex++;
    kGlobalIndex++;
  }

  for (; (kIndex < KERNEL_Q_K_ELEMS_PER_GRID) && (kGlobalIndex < numK);
       kIndex += 2, kGlobalIndex += 2) {
    float expArg = PIx2 * (ck[kIndex].Kx * sX +
			   ck[kIndex].Ky * sY +
			   ck[kIndex].Kz * sZ);
    sQr += ck[kIndex].PhiMag * cos(expArg);
    sQi += ck[kIndex].PhiMag * sin(expArg);

    int kIndex1 = kIndex + 1;
    float expArg1 = PIx2 * (ck[kIndex1].Kx * sX +
			    ck[kIndex1].Ky * sY +
			    ck[kIndex1].Kz * sZ);
    sQr += ck[kIndex1].PhiMag * cos(expArg1);
    sQi += ck[kIndex1].PhiMag * sin(expArg1);
  }

  Qr[xIndex] = sQr;
  Qi[xIndex] = sQi;

#else

  float sX;
  float sY;
  float sZ;
  float sQr;
  float sQi;

  // Determine the element of the X arrays computed by this thread
  int xIndex = get_group_id(0)*KERNEL_Q_THREADS_PER_BLOCK + get_local_id(0);

  // Read block's X values from global mem to shared mem
  sX = x[xIndex];
  sY = y[xIndex];
  sZ = z[xIndex];
  sQr = Qr[xIndex];
  sQi = Qi[xIndex];

  int kIndex = 0;
  for (; (kIndex < numK) && (kGlobalIndex < numK);
       kIndex ++, kGlobalIndex ++) {
    float expArg = PIx2 * (ck[kIndex].Kx * sX +
			   ck[kIndex].Ky * sY +
			   ck[kIndex].Kz * sZ);
    sQr += ck[kIndex].PhiMag * cos(expArg);
    sQi += ck[kIndex].PhiMag * sin(expArg);
  }

  Qr[xIndex] = sQr;
  Qi[xIndex] = sQi;

#endif  /* UNROLL_2X */

#endif
}
